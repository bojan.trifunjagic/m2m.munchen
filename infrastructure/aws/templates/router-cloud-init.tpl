#cloud-config
write_files:
-   encoding: b64
    path:  /etc/telegraf/telegraf.d/telegraf.conf
    content: "${production_telegraf_content}"
    permissions: '0644'

package_upgrade: true

packages:
  - awscli
  - nodejs
  - npm

runcmd:
  - wget https://dl.influxdata.com/telegraf/releases/telegraf_1.6.3-1_amd64.deb
  - sudo dpkg -i telegraf_1.6.3-1_amd64.deb
  - mkdir -p /home/ubuntu/router
  - AWS_ACCESS_KEY_ID=${aws_access_key_id} AWS_SECRET_ACCESS_KEY=${aws_secret_access_key} aws s3 cp "s3://${router_service_bucket}/${router_service_tar}" /home/ubuntu/router/
  - chown -R ubuntu /home/ubuntu/router
  - cd /home/ubuntu/router/
  - echo CURRENT DIR
  - pwd
  - tar xzf ${router_service_tar}
  - LOBSTERS_DNS_NAME=${lobsters_dns_name} MODLOG_DNS_NAME=${modlog_dns_name} node index.js
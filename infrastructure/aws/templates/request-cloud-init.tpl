#cloud-config
write_files:
-   encoding: b64
    path:  /home/ubuntu/app/application-production.properties
    content: "${request_service_properties}"
    permissions: '0644'
-   encoding: b64
    path:  /etc/telegraf/telegraf.d/telegraf.conf
    content: "${production_telegraf_content}"
    permissions: '0644'

package_upgrade: true

packages:
  - awscli
  - openjdk-8-jdk

runcmd:
  - wget https://dl.influxdata.com/telegraf/releases/telegraf_1.6.3-1_amd64.deb
  - sudo dpkg -i telegraf_1.6.3-1_amd64.deb
  - mkdir -p /home/ubuntu/app
  - AWS_ACCESS_KEY_ID=${aws_access_key_id} AWS_SECRET_ACCESS_KEY=${aws_secret_access_key} aws s3 cp "s3://${request_service_bucket}/${request_service_jar}" /home/ubuntu/app/
  - cd /home/ubuntu/app && java -jar "/home/ubuntu/app/${request_service_jar}" --spring.profiles.active=production

InfluxDB::Rails.configure do |config|
    config.influxdb_database = ${database}
    config.influxdb_username = ""
    config.influxdb_password = ""
    config.influxdb_hosts    = ${urls}
    config.influxdb_port     = 8086
end
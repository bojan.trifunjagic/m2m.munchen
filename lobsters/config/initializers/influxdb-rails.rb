InfluxDB::Rails.configure do |config|
    config.influxdb_database = "test"
    config.influxdb_username = ""
    config.influxdb_password = ""
    config.influxdb_hosts    = ["influx"]
    config.influxdb_port     = 8086
end
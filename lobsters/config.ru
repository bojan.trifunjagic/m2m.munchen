# This file is used by Rack-based servers to start the application.
require 'zipkin-tracer'

require ::File.expand_path('../config/environment',  __FILE__)

zipkin_config = {
    service_name: 'lobsters',
    service_port: 300,
    sample_rate: 1,
    sampled_as_boolean: false,
    log_tracing_rate: true,
    json_api_host: "http://#{Lobsters::Application.config.tracing_host}:9411/api/v1/spans"
}

use ZipkinTracer::RackHandler, zipkin_config

run Rails.application

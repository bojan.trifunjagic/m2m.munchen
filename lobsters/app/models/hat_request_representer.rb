require 'roar/json'

class HatRequestRepresenter < OpenStruct
    include Roar::JSON
    property :subject
    property :object
    collection :evidence
end
  